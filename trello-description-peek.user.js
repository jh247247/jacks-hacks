// ==UserScript==
// @name         Trello Description peek
// @namespace    http://tampermonkey.net/
// @version      0.1.1
// @description  Peek {n} lines of the description on the front of an unexpanded trello card
// @author       Jack Hosemans
// @match        https://*trello.com/b/*
// @include      https://*trello.com/b/*
// @match        https://*trello.com/c/*
// @include      https://*trello.com/c/*
// @require      https://cdn.rawgit.com/showdownjs/showdown/1.8.0/dist/showdown.min.js
// @grant        none
// ==/UserScript==

// make a DOM mutation listener, I made a note about this on the card.
var cardMap = {};

const expandRegex = /;\s*expand\s*[0-9]*/i;
var converter = new showdown.Converter();
var immUpdateHref = null;

var css = `
.expand-section ul{
    margin-bottom: inherit;
    list-style: circle;
    list-style-position: outside;
    margin-left: 16px;
}
`;

/*document.styleSheets[0].addRule('.expand-section::before',
                                'content: "";white-space: pre');*/

function updateCardWithJson(cardElement) {
    console.log("Getting json for card at: " + cardElement.href);
    $.getJSON(cardElement.href + '.json', function(d) {
        cardMap[d.shortLink] = d;
        updateCard(cardElement);
        immUpdateHref = null;
    });
}

function updateCard(cardElement) {
        console.log("Updating card at: " + cardElement.href);


        var id = cardElement.href.split('/')[4];
        var desc = cardMap[id].desc;
        var expandString = expandRegex.exec(desc);
        var lastCardElement = $(cardElement).children('.list-card-details')[0].lastElementChild;
        if(expandString === null) {
            // hide dom element from being displayed
            if(lastCardElement.classList.contains('expand-section')) {
                lastCardElement.style.display = 'none';
            }
            return;
        }

        // slice until expand comment found
        var markdown = desc.slice(0, expandString.index);

        if(!lastCardElement.classList.contains('expand-section')){
            var expandDiv = document.createElement('div');
            expandDiv.style.clear = 'both';
            expandDiv.className = 'expand-section';
            expandDiv.style.paddingBottom = '4px';

            $(expandDiv).insertAfter(lastCardElement);
            console.log("inserting new element!");
            lastCardElement = expandDiv;
        }
        lastCardElement.innerHTML = converter.makeHtml(markdown);
        lastCardElement.style.display = 'block';
}

function updateAllCards(force) {
    var cards = jQuery(".list-card");
    if(window.location.href.split('/')[3] === 'c') {
        immUpdateHref = window.location.href;
        console.log("Have to update: " + window.location.href);
    }

    for(var i = 0; i < cards.length; i++) {
        if(force) {
            updateCard(cards[i]);
        } else if(cards[i].href  == immUpdateHref && window.location.href.split('/')[3] !== 'c') {
            updateCardWithJson(cards[i]);
        }
    }
}

function seedCardMap() {
    // get all data related to the board
    $.getJSON(window.location.href + '.json', function(d) {
        for(var i = 0; i < d.cards.length; i++) {
            cardMap[d.cards[i].shortLink] = d.cards[i];
        }
        updateAllCards(true);
    });
}

function insertCss( code ) {
    var style = document.createElement('style');
    style.type = 'text/css';

    if (style.styleSheet) {
        // IE
        style.styleSheet.cssText = code;
    } else {
        // Other browsers
        style.innerHTML = code;
    }

    document.getElementsByTagName("head")[0].appendChild( style );
}

insertCss(css);
seedCardMap();


setInterval(updateAllCards, 1000);

