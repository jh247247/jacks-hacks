// ==UserScript==
// @name         Trello fuzzy card links
// @namespace    http://tampermonkey.net/
// @version      0.7.2
// @description  Link Trello cards, even between boards
// @author       Jack Hosemans
// @match        https://*trello.com/b/*
// @include      https://*trello.com/b/*
// @match        https://*trello.com/c/*
// @include      https://*trello.com/c/*
// @grant        none
// @run-at       document-idle
// @require      https://raw.githubusercontent.com/lodash/lodash/4.17.5/dist/lodash.min.js
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @require      https://rawcdn.githack.com/nextapps-de/flexsearch/master/dist/flexsearch.min.js
// @updateUrl    https://bitbucket.org/jh247247/jacks-hacks/raw/master/trello-fuzzy-card-links.user.js
// ==/UserScript==

// NOTE: make sure to add a card with the title 'connected boards' if you want to link to cards from other boards
// add urls to the boards in the description of that card

// when user is typing in textarea, match previous word to card map
// if name matches, show some menu to link card
// if tab pressed add link, make menu clickable
// clear data if url changes to different board (or card in different board...)

// map of connected boards
// { boardName -> { cardName -> url } }

console.log("Fuzzy card links starting!");

const descParentSelector = '.description-edit.edit'

var connectedBoards = [];
var connectedBoardCards = {};
var flexsearcher;// = FlexSearch.create({doc: { id: 'id', field: 'name' }});
var fuzzyResults = [];
var fuzzyselection = 0;
var dismiss = false;
var currentBoard = null;

const updateConnectedBoards = (callback) => {
    // get the config out of the card Data
    var configCard = _.filter(connectedBoardCards[currentBoard].cards, function (d) {
        return /connected\s*boards/i.test(d.title);
    });

    if(configCard.length === 0) return;

    console.log('loading connected boards!');
    $.getJSON(`https://trello.com/c/${configCard[0].id}.json`, function(j) {
        connectedBoards = j.desc.replace( /\n/g, " " ).split( " " );

        _.map(connectedBoards, function(d) {
            if(d.startsWith('https://trello.com/b')) {
                updateBoard(d + '.json');
            }
        });
    });
}

const updateSearcher = (cards) => {
    if(!flexsearcher) {
        flexsearcher = FlexSearch.create('match', {doc: { id: 'id', field: 'title' }});
    }
    flexsearcher.add(cards);
}

function updateCurrentBoard() {
    connectedBoards = [];
    connectedBoardCards = {};
    const url = `${window.location.origin}${$('[data-react-beautiful-dnd-drag-handle="0"] > div > a').eq(0).attr('href')}.json`;
    updateBoard(url, () => {
        currentBoard = connectedBoards[0];
        updateConnectedBoards();
    });
}

// seed the map by parsing the JSON
function updateBoard(url, callback) {
    $.getJSON(url, function(data) {
        var cards = data.cards.filter(c => !c.closed
                                      && !data.lists.find(list => list.id === c.idList).closed)
        .map(d => {
            return {
                id: d.shortLink,
                title: d.name,
                board: data.name
            };
        });

        updateSearcher(cards);
        connectedBoards.push(data.name);
        connectedBoardCards[data.name] = {'cards': cards, 'name': data.name};
        callback && callback();
    });
}

// insert text at cursor, replace previous word
function insertAtCursor(el, str) {
    // check if textarea
        const valueArr = el.value.substr(0,el.selectionStart).split(' ');
        const replLen = valueArr[valueArr.length-1].length;

        const oldSelectionStart = el.selectionStart;

        el.value = el.value.substr(0,el.selectionStart- replLen) +
                str + ' ' +
                el.value.substr(el.selectionStart);

        el.selectionStart = oldSelectionStart - replLen + str.length;
        el.selectionEnd = el.selectionStart
}

const getCursorXY = (input, selectionPoint) => {
    const {
        offsetLeft: inputX,
        offsetTop: inputY,
    } = input
    // create a dummy element that will be a clone of our input
    const div = document.createElement('div')
    // get the computed style of the input and clone it onto the dummy element
    const copyStyle = getComputedStyle(input)
    for (const prop of copyStyle) {
        div.style[prop] = copyStyle[prop]
    }
    // we need a character that will replace whitespace when filling our dummy element if it's a single line <input/>
    const swap = '.'
    const inputValue = input.tagName === 'INPUT' ? input.value.replace(/ /g, swap) : input.value
    // set the div content to that of the textarea up until selection
    const textContent = inputValue.substr(0, selectionPoint)
    // set the text content of the dummy element div
    div.textContent = textContent
    if (input.tagName === 'TEXTAREA') div.style.height = 'auto'
    // if a single line input then the div needs to be single line and not break out like a text area
    if (input.tagName === 'INPUT') div.style.width = 'auto'
    // create a marker element to obtain caret position
    const span = document.createElement('span')
    // give the span the textContent of remaining content so that the recreated dummy element is as close as possible
    span.textContent = inputValue.substr(selectionPoint) || '.'
    // append the span marker to the div
    div.appendChild(span)
    // append the dummy element to the body
    document.body.appendChild(div)
    // get the marker position, this is the caret position top and left relative to the input
    const { offsetLeft: spanX, offsetTop: spanY } = span
    // lastly, remove that dummy element
    // NOTE:: can comment this out for debugging purposes if you want to see where that span is rendered
    document.body.removeChild(div)
    // return an object with the x and y of the caret. account for input positioning so that you don't need to wrap the input
    return {
        x: inputX + spanX,
        y: inputY + spanY,
    }
}

function generateSuggestion(textarea, card, selected) {
    return `<div class="suggestion"><div class="sugg-card">${card.title}</div><div class="sugg-board">${card.board}</div></div>`;
}

function injectIfMatch(event) {
    const selection = document.getSelection();
    var desc = $(selection.anchorNode).find('textarea');

    var suggestDiv = $('.card-suggestions');

    // not editing, hide suggestions and return.
    if(desc.length !== 1 || !flexsearcher) {
        suggestDiv.css('display', 'none');
        return;
    }

    // grab the data from the textarea and figure out what word we need to search/replace
    var preWord = null;
    var cursorPosition = null;
    var preText = null;
    if(desc[0].selectionStart) {
        // textarea
        cursorPosition = desc[0].selectionStart;
        preText = desc[0].value.substring(0,cursorPosition).replace( /\n/g, " " ).split(' ');
        preWord = preText[preText.length - 1];
    } else {
        suggestDiv.css('display', 'none');
        console.log("No selection!??");
        return;
    }

    // reflect user dismiss
    if(dismiss
       || fuzzyResults.length === 0
       || !desc[0].value.charAt(cursorPosition-1).match(/\w/g))  {
        suggestDiv.css('display', 'none');
    } else if(fuzzyResults.length !== 0) {
        const descCoords = desc.offset();
        suggestDiv.css('display','block');
        const caretPos = getCursorXY(desc[0], desc[0].selectionStart);
        const actualY = caretPos.y - window.innerHeight + descCoords.top + 16;
        suggestDiv.css('left', caretPos.x + descCoords.left + 4);
        suggestDiv.css('top', actualY);
    }

    if(event.type === 'keyup'
       && (event.key === 'Tab'
           || (fuzzyselection !== 0 && event.key === 'Enter')
       && !dismiss)
       ) {
        event.preventDefault();
        return;
    }

    // insert into textarea!
    if(event.type === 'keydown' &&
       (event.key === 'Tab'
        || (fuzzyselection !== 0 && event.key === 'Enter'))
        && !dismiss
        && fuzzyResults[fuzzyselection]) {
        event.preventDefault();
        console.log(`Inserting ${JSON.stringify(fuzzyResults[fuzzyselection])}`);
        suggestDiv.css('display', 'none');
        insertAtCursor(desc[0], `https://trello.com/c/${fuzzyResults[fuzzyselection].id}`);
        fuzzyResults = [];
        fuzzyselection = 0;
        return;
    }

    // dismiss suggestion
    if(event.key === 'Escape') {
        console.log('Dismissing suggestion dialog');
        dismiss = true;
        event.preventDefault();
        return;
    }

    // typing a new word! reinstate suggestions.
    if(event.key === ' ' && dismiss) {
        console.log('Reinstating suggestion dialog');
        dismiss = false;
        return;
    }

    console.log('attempting to match!');
    fuzzyResults = flexsearcher.search(preWord);

    if(preWord.length !== 0) {
        if(event.key === "ArrowDown" && fuzzyselection < Math.min(10, fuzzyResults.length)) {
            if(event.type === 'keyup') {
                fuzzyselection = fuzzyselection + 1;
            }
            event.preventDefault();
        } else if(event.key === "ArrowUp" && fuzzyselection > 0) {
            if(event.type === 'keyup') {
                fuzzyselection = fuzzyselection - 1;
            }
            event.preventDefault();
        }

        suggestDiv.empty();

        for(var i = 0; i < Math.min(10, fuzzyResults.length); i++) {
            suggestDiv.append(generateSuggestion(desc[i], fuzzyResults[i], fuzzyselection === i));
        }

        suggestDiv.children().eq(fuzzyselection).css('background-color', 'gainsboro');
        suggestDiv.children().css('white-space', 'nowrap').css('display', 'flex');
        $('.suggestion .sugg-card')
            .css('flex', '1 1 auto')
            .css('overflow', 'hidden')
            .css('text-overflow', 'ellipsis')
            .css('padding-right', '16px');
        $('.suggestion .sugg-board')
            .css('align-self', 'flex-end')
            .css('font-weight', 'bold')
            .css('flex', '0 0 auto');
    }
}

document.addEventListener('mouseup', () => {
    setTimeout(
        () => {
            const suggestion = $('.card-suggestions');
            const description = $(document.getSelection().anchorNode).find('textarea');

            description.off('keydown', injectIfMatch);
            description.off('keyup', injectIfMatch);

            if (!description.length) {
                suggestion.css('display', 'none');
            } else if(description.length){
                description.on('keydown', injectIfMatch);
                description.on('keyup', injectIfMatch);
            }
        }, 10);
});

const suggestDiv = $('<div class=\'card-suggestions\'></div>')
$('body').append(suggestDiv);
suggestDiv.css('display', 'none');
suggestDiv.css('flex-direction', 'column');
suggestDiv.css('background', '#edeff0');
suggestDiv.css('color', '#4d4d4d');
suggestDiv.css('position', 'absolute');
suggestDiv.css('zIndex',30);
suggestDiv.css('top', '0');
suggestDiv.css('left', '0');
suggestDiv.css('max-width', '40%');
suggestDiv.css('border', '2px solid gray');
suggestDiv.css('border-radius', '3px');
suggestDiv.css('minWidth', '300px');

setTimeout(updateCurrentBoard, 5000);