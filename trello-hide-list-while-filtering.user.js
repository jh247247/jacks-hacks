
// ==UserScript==
// @name         Trello filtering enhancements
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Misc trello filtering enhancements
// @author       You
// @match        https://*trello.com/b/*
// @match        https://*trello.com/c/*
// @include      https://*trello.com/b/*
// @include      https://*trello.com/c/*
// @run-at       document-idle
// @grant        none
// @updateUrl    https://bitbucket.org/jh247247/jacks-hacks/raw/master/trello-hide-list-while-filtering.user.js
// ==/UserScript==

const timeout = 500;

function hideEmptyLists() {
    var closeList = function (list) {
        list.style.maxWidth     = '4px';
        list.style.display      = 'none';
    };

    var openList = function (list) {
        list.style.overflow     = 'hidden';
        list.style.maxWidth     = '250px';
        list.style.display      = 'inline-block';
    };

    var lists = document.getElementById('board').querySelectorAll('.list-wrapper');
    var filtering = !!!document.querySelector('.js-filter-cards-indicator.hide');
    var activeCard = document.querySelector('.active-card');
    var isActiveCardDragging = activeCard !== null && activeCard.style && activeCard.style.position === 'absolute';

    if(!filtering || isActiveCardDragging) {
        for(var i = 0; i < lists.length; i++) {
            openList(lists[i]);
        }
        setTimeout(hideEmptyLists, timeout);
        return;
    }

    if(filtering) {
        // attach listener for filter input blur
        var filterTextBox = document.querySelector('.board-menu .js-input.js-autofocus');
        var boardMenuCloseButton = document.querySelector('.board-menu .icon-close.js-hide-sidebar');
        if(filterTextBox !== null) {
            filterTextBox.onblur = function() {
                boardMenuCloseButton.click();
            };
        }
    }


    for (var i = 0; i < lists.length; i++) {
        (function () {
            var list    = lists[i];
            var cards   = list.querySelectorAll('.list-card');
            var hiddenCards   = list.querySelectorAll('.list-card.hide');
            if(cards.length === hiddenCards.length && filtering) {
                closeList(list);
            } else {
                openList(list);
            }
        })();
    }

    setTimeout(hideEmptyLists, timeout);
}

// close board menu when we press escape
document.addEventListener('keydown', function(event) {
    if(event.key === 'Escape') {
        var boardMenuCloseButton = document.querySelector('.board-menu .icon-close.js-hide-sidebar');
        console.log(boardMenuCloseButton);
        if(boardMenuCloseButton.offsetParent !== null) {
            boardMenuCloseButton.click();
            return;
        }

        var filterIndicatorClose = document.querySelector('.js-filter-card-clear');
        if(filterIndicatorClose) {
            filterIndicatorClose.click();
        }
    }
});

setTimeout(hideEmptyLists,1000);