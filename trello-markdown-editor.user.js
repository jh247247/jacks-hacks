// ==UserScript==
// @name         Trello Markdown WYSIWYG
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  Insert a WYSIWYG editor for trello cards
// @author       Jack Hosemans
// @match        https://*trello.com/b/*
// @match        https://*trello.com/c/*
// @include      https://*trello.com/b/*
// @include      https://*trello.com/c/*
// @grant        GM_addStyle
// @require      http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js
// @require      https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js
// @updateUrl    https://bitbucket.org/jh247247/jacks-hacks/raw/master/trello-markdown-editor.user.js
// ==/UserScript==
(function() {
    'use strict';
    console.log("Loaded WYSIWYG");

    GM_addStyle('.CodeMirror pre.CodeMirror-line { box-shadow: none; }');

    $('head').append('<link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">');
    document.addEventListener('mouseup', () => {
        setTimeout(
            () => {
                const mdEditor = $(".CodeMirror")
                const el = $(".js-description-draft")[0];
                if(el && !mdEditor[0]) {
                    console.log("TRYING TO ADD WYSIWYG");
                    var simplemde = new SimpleMDE({ element: el,
                                                   autofocus: true,
                                                   forceSync: true,
                                                   initialValue: el.value,
                                                   hideIcons: ["guide", "fullscreen", "side-by-side"],
                                                   parsingConfig: {
                                                       allowAtxHeaderWithoutSpace: true,
                                                       underscoresBreakWords: true
                                                   },
                                                   tabSize: 4
                                                  });

                    // prevent clicks on editor/toolbar from closing edit mode
                    $('.CodeMirror').on('click', (e) => {
                        e.preventDefault();
                        e.stopPropagation();
                    });

                    $('.editor-toolbar').on('click', (e) => {
                        e.preventDefault();
                        e.stopPropagation();
                    });
                }

                if(!$('.edit-controls')[0]) {
                    $('.CodeMirror').remove();
                    $('.editor-statusbar').remove();
                    $('.editor-toolbar').remove();
                    $('.editor-preview-side').remove();
                }
            }, 0);
    });
})();